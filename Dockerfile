# Production Stage
FROM alpine:latest

RUN apk --no-cache add ca-certificates

COPY ./service ./service

RUN chmod +x ./service

CMD ["./service"]
