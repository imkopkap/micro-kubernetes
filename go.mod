module gitlab.com/imkopkap/micro-kubernetes

go 1.13

require (
	github.com/golang/protobuf v1.3.2
	github.com/micro/examples v0.2.0
	github.com/micro/go-micro v1.18.0
	github.com/micro/go-micro/v2 v2.0.0
	github.com/micro/go-plugins v1.1.2-0.20190710094942-bf407858372c
	github.com/micro/go-plugins/broker/rabbitmq/v2 v2.0.1
	github.com/micro/go-plugins/registry/kubernetes/v2 v2.0.1
	github.com/micro/go-plugins/transport/nats/v2 v2.0.1
	github.com/micro/protoc-gen-micro/v2 v2.0.0 // indirect
)
